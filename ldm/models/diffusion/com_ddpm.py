"""SAMPLING ONLY."""

import torch
import numpy as np
from tqdm import tqdm
from functools import partial

from ldm.modules.diffusionmodules.util import make_ddim_sampling_parameters, make_ddim_timesteps, noise_like, \
    extract_into_tensor


class DDPMSampler(object):
    def __init__(self, model, schedule="linear", **kwargs):
        super().__init__()
        self.e_t = None
        self.model = model
        self.ddpm_num_timesteps = model.num_timesteps
        self.schedule = schedule

    def register_buffer(self, name, attr):
        if type(attr) == torch.Tensor:
            if attr.device != torch.device("cuda"):
                attr = attr.to(torch.device("cuda"))
        setattr(self, name, attr)

    def make_schedule(self):

        alphas_cumprod = self.model.alphas_cumprod
        assert alphas_cumprod.shape[0] == self.ddpm_num_timesteps, 'alphas have to be defined for each timestep'
        to_torch = lambda x: x.clone().detach().to(torch.float32).to(self.model.device)

        self.register_buffer('betas', to_torch(self.model.betas))
        self.register_buffer('alphas_cumprod', to_torch(alphas_cumprod))
        self.register_buffer('alphas_cumprod_prev', to_torch(self.model.alphas_cumprod_prev))

        # calculations for diffusion q(x_t | x_{t-1}) and others
        self.register_buffer('sqrt_alphas_cumprod', to_torch(np.sqrt(alphas_cumprod.cpu())))
        self.register_buffer('sqrt_one_minus_alphas_cumprod', to_torch(np.sqrt(1. - alphas_cumprod.cpu())))
        self.register_buffer('log_one_minus_alphas_cumprod', to_torch(np.log(1. - alphas_cumprod.cpu())))
        self.register_buffer('sqrt_recip_alphas_cumprod', to_torch(np.sqrt(1. / alphas_cumprod.cpu())))
        self.register_buffer('sqrt_recipm1_alphas_cumprod', to_torch(np.sqrt(1. / alphas_cumprod.cpu() - 1)))

    @torch.no_grad()
    def sample(self,
               batch_size,
               shape,
               conditioning=None,
               mask=None,
               x0=None,
               score_corrector=None,
               corrector_kwargs=None,
               x_T=None,
               log_every_t=100,
               init_image=None,
               unconditional_guidance_scale=1.,
               classifier_guidance_scale=0.,
               unconditional_conditioning=None,
               # this has to come in the same format as the conditioning, # e.g. as encoded tokens, ...
               **kwargs
               ):
        if conditioning is not None:
            if isinstance(conditioning, dict):
                cbs = conditioning[list(conditioning.keys())[0]].shape[0]
                if cbs != batch_size:
                    print(f"Warning: Got {cbs} conditionings but batch-size is {batch_size}")
            else:
                if conditioning.shape[0] != batch_size:
                    print(f"Warning: Got {conditioning.shape[0]} conditionings but batch-size is {batch_size}")

        # sampling
        C, H, W = shape
        size = (batch_size, C, H, W)
        print(f'Data shape for DDPM sampling is {size}')

        samples, intermediates = self.ddpm_sampling(conditioning, size,
                                                    mask=mask, x0=x0,
                                                    score_corrector=score_corrector,
                                                    corrector_kwargs=corrector_kwargs,
                                                    x_T=x_T,
                                                    log_every_t=log_every_t,
                                                    init_image=init_image,
                                                    unconditional_guidance_scale=unconditional_guidance_scale,
                                                    classifier_guidance_scale=classifier_guidance_scale,
                                                    unconditional_conditioning=unconditional_conditioning,
                                                    )
        return samples, intermediates

    @torch.no_grad()
    def ddpm_sampling(self, cond, shape,
                      x_T=None, mask=None, x0=None, log_every_t=100, score_corrector=None, corrector_kwargs=None,
                      classifier_guidance_scale=0., init_image=None,
                      unconditional_guidance_scale=1., unconditional_conditioning=None, ):
        device = self.model.betas.device
        b = shape[0]
        if x_T is None:
            img = torch.randn(shape, device=device)
        else:
            img = x_T

        timesteps = self.ddpm_num_timesteps
        compressed_img = x_T

        intermediates = {'x_inter': [img], 'pred_x0': [img]}
        time_range = reversed(range(0, timesteps))
        total_steps = timesteps
        print(f"Running DDPM Sampling with {total_steps} timesteps")

        iterator = tqdm(time_range, desc='DDPM Sampler', total=total_steps)

        # for i in tqdm(reversed(range(0, self.num_timesteps)), desc='Sampling t', total=self.num_timesteps):
        #     img = self.p_sample(img, torch.full((b,), i, device=device, dtype=torch.long),
        #                         clip_denoised=self.clip_denoised)
        #     if i % self.log_every_t == 0 or i == self.num_timesteps - 1:
        #         intermediates.append(img)
        # if return_intermediates:
        #     return img, intermediates
        # return img

        for i, step in enumerate(iterator):
            index = total_steps - i - 1
            ts = torch.full((b,), step, device=device, dtype=torch.long)

            if mask is not None:
                assert x0 is not None
                img_orig = self.model.q_sample(x0, ts)  # TODO: deterministic forward pass?
                img = img_orig * mask + (1. - mask) * img

            outs = self.p_sample_ddpm(img, cond, ts, index=index,
                                      score_corrector=score_corrector,
                                      corrector_kwargs=corrector_kwargs,
                                      init_image=init_image,
                                      compressed_img=compressed_img,
                                      unconditional_guidance_scale=unconditional_guidance_scale,
                                      classifier_guidance_scale=classifier_guidance_scale,
                                      unconditional_conditioning=unconditional_conditioning)
            img, pred_x0 = outs

            dec_img = self.model.decode_first_stage(img)  # decode image

            compressor.encoder(dec_img)  # compress image
            with open('****.bin', 'rb') as file:
                compressed_data = file.read()
            compressed_img = np.frombuffer(compressed_data, dtype=np.float32)
            compressed_img = torch.from_numpy(compressed_img)
            compressed_img = torch.clamp((compressed_img + 1.0) / 2.0, min=0.0, max=1.0)  # x_hat_t

            if index % log_every_t == 0 or index == total_steps - 1:
                intermediates['x_inter'].append(img)
                intermediates['pred_x0'].append(pred_x0)

        return img, intermediates

    @torch.no_grad()
    def p_sample_ddpm(self, x, c, t, e_t, repeat_noise=False,
                      score_corrector=None, corrector_kwargs=None, classifier_guidance_scale=0., init_image=None,
                      compressed_img=None,
                      unconditional_guidance_scale=1., unconditional_conditioning=None):

        b, *_, device = *x.shape, x.device
        self.e_t = e_t
        if unconditional_conditioning is None or unconditional_guidance_scale == 1.:
            e_t = self.model.apply_model(x, t, c)
        else:
            x_in = torch.cat([x] * 2)
            t_in = torch.cat([t] * 2)
            c_in = torch.cat([unconditional_conditioning, c])
            e_t_uncond, e_t = self.model.apply_model(x_in, t_in, c_in).chunk(2)
            self.e_t = e_t_uncond + unconditional_guidance_scale * (e_t - e_t_uncond)

        if score_corrector is not None:
            assert self.model.parameterization == "eps"
            self.e_t = score_corrector.modify_score(self.model, e_t, x, t, c, **corrector_kwargs)

        model_mean, _, model_log_variance = self.p_mean_variance(x=x, t=t, clip_denoised=True,
                                                                 classifier_guidance_scale=classifier_guidance_scale,
                                                                 init_image=init_image, compressed_img=compressed_img)
        noise = noise_like(x.shape, device, repeat_noise)
        nonzero_mask = (1 - (t == 0).float()).reshape(b, *((1,) * (len(x.shape) - 1)))
        return model_mean + nonzero_mask * (0.5 * model_log_variance).exp() * noise

    def p_mean_variance(self, x, t, clip_denoised: bool, classifier_guidance_scale=0., init_image=None,
                        compressed_img=None):
        model_out = self.e_t
        if self.model.parameterization == "eps":
            x_recon = self.predict_start_from_noise(x, t=t, noise=model_out)
        elif self.model.parameterization == "x0":
            x_recon = model_out
        if clip_denoised:
            x_recon.clamp_(-1., 1.)

        model_mean, posterior_variance, posterior_log_variance = self.q_posterior(x_start=x_recon, x_t=x, t=t,
                                                                                  classifier_guidance_scale=classifier_guidance_scale,
                                                                                  init_image=init_image,
                                                                                  compressed_img=compressed_img)
        return model_mean, posterior_variance, posterior_log_variance

    def predict_start_from_noise(self, x_t, t, noise):
        return (
                extract_into_tensor(self.sqrt_recip_alphas_cumprod, t, x_t.shape) * x_t -
                extract_into_tensor(self.sqrt_recipm1_alphas_cumprod, t, x_t.shape) * noise
        )

    def q_posterior(self, x_start, x_t, t, classifier_guidance_scale=0., init_image=None, compressed_img=None):

        posterior_variance = extract_into_tensor(self.posterior_variance, t, x_t.shape)

        x_hat_g = init_image  # xˆg
        x_hat_t = compressed_img  # xˆt

        img_guidance = torch.abs(x_hat_g - x_hat_t)  # 计算 |xˆg - xˆt|

        gradient = torch.autograd.grad(img_guidance)  # 计算 ∇|xˆg - xˆt|

        posterior_mean = (
                extract_into_tensor(self.posterior_mean_coef1, t, x_t.shape) * x_start +
                extract_into_tensor(self.posterior_mean_coef2, t, x_t.shape) * x_t -
                classifier_guidance_scale * posterior_variance * gradient
        )
        posterior_log_variance_clipped = extract_into_tensor(self.posterior_log_variance_clipped, t, x_t.shape)
        return posterior_mean, posterior_variance, posterior_log_variance_clipped
