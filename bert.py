from transformers import BertTokenizer

BERT_PATH = '/bert-base-uncased'

tokenizer = BertTokenizer.from_pretrained(BERT_PATH)

print(tokenizer.tokenize('To be, or not to be !'))
